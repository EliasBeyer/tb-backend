


// must match backend python (all caps identifiers)
const TimerState = {
    Paused: "PAUSED",
    Running: "RUNNING",
}

// must match backend python (all caps identifiers)
const PresentationState = {
    NoPresentation : "NO_PRESENTATION", // no presentation opened
    PresentationOpen : "PRESENTATION_OPEN", // edit mode
    SlideShowActive : "ACTIVE_SLIDESHOW", // presenter mode, slideshow active
}

async function toggleTimer()
{
    const newState = await pywebview.api.onBtnToggleTimerClicked()

    // console.log( `toggleTimer: ${newState}` )

    setBtnTimerState( newState )
}

function setBtnTimerState( timerState )
{
    console.assert( timerState )
    // console.log( `setBtnTimerState: ${timerState}` )

    const btnTimer = document.getElementById( "btn-toggle-timer" )

    switch( timerState )
    {
        case TimerState.Paused: {
            btnTimer.innerText = "Start"
        } break
        case TimerState.Running: {
            btnTimer.innerText = "Stop"
        } break
        default: {
            console.error( `Timer-State of '${ timerState }' doesn't match the backend in python!` )
        } break
    }
}

async function resetTimer()
{
    // console.log( `resetTimer` )

    const state = await pywebview.api.onBtnResetTimerClicked()

    setBtnTimerState( state )
}

