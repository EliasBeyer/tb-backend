ARG PY_VERSION=3.11

# See: https://github.com/webcomics/pywine/blob/main/Dockerfile
FROM docker.io/tobix/pywine:$PY_VERSION

RUN apt-get update -y

WORKDIR /app

# Add requirements.txt to tracked files
ADD ./empFeedbackTool/requirements.txt requirements.txt

# Install common Python Packages

RUN wine python --version
RUN wine python -m pip install --upgrade pip
RUN wine python -m pip install setuptools pyinstaller wheel
RUN wine python -m pip install pytest pytest-cov
RUN wine python -m pip install -r requirements.txt
