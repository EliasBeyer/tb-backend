
import time
from timebuddy.Timer import Timer
from timebuddy.TimerState import TimerState


class Test_Timer :

    def test_initially_paused( self ) :
        t = Timer()
        assert t.getState() == TimerState.Paused

    def test_start_running( self ) :
        t = Timer()
        t.start()
        assert t.getState() == TimerState.Running

    def test_stop_paused( self ) :
        t = Timer()
        t.stop()
        assert t.getState() == TimerState.Paused

    def test_toggle_changes_state( self ) :
        t = Timer()
        t.toggle()
        assert t.getState() == TimerState.Running
        t.toggle()
        assert t.getState() == TimerState.Paused

    def test_elapsed_time( self ) :
        t = Timer()
        for i in range( 1 , 3 ) :
            t.start()
            time.sleep( 1.0 )
            t.stop()
            elapsedSeconds = t.getElapsedSeconds()
            assert elapsedSeconds == i

    def test_reset( self ) :
        t = Timer()
        t.start()
        time.sleep( 1.0 )
        t.reset()
        assert t.getElapsedSeconds() == 0
        assert t.getState() == TimerState.Paused

