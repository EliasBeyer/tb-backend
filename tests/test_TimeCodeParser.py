
import pytest
from timebuddy.Presentation.SlideNote import SlideNote
from timebuddy.Presentation.TimeCodeParser import TimeCodeParser
from timebuddy.Settings import Settings


class Test_TimeCodeParser :

    @pytest.mark.parametrize( "note , expectedSeconds" , [
        ( "/time=12:34+56" , 12 * 60 + 34 + 56 ) ,    # Test with positive delay
        ( "/time=05:23-12" , 5 * 60 + 23 - 12 ) ,     # Test with negative delay
        ( "/time=09:45" , 9 * 60 + 45 ) ,             # Test with no delay
        ( "/time = 09:45" , 9 * 60 + 45 ) ,           # Test with whitespace
        ( "/Time=09:45" , 9 * 60 + 45 ) ,             # Test with different lettercasing
        ( "/time=3:09+5" , 3 * 60 + 9 + 5 ) ,         # Test with single-digit minutes
        ( "/time=03:9+5" , 3 * 60 + 9 + 5 ) ,         # Test with single-digit seconds
    ] )
    def test_valid_extractTimeFromNotes( self , note , expectedSeconds ) :
        slideNote = SlideNote()
        slideNote.text = note
        timeCode = TimeCodeParser.getTimeCodeFromNotes( slideNote )

        assert timeCode.totalTimeInSeconds == expectedSeconds
        assert timeCode.lineNumber == 0

    @pytest.mark.parametrize( "note" , [
        ( "/invalid_time=12:34" ) ,               # Test with invalid time pattern
        ( "/time=-12:34" ) ,                      # Test with negative minutes
        ( "No time information" ) ,               # Test with no time information in the note
        ( None ) ,                                # Test with None input
        ( "/time=/n01:30" ) ,                     # Test with newline
    ] )
    def test_invalid_extractTimeFromNotes( self , note ) :
        slideNote = SlideNote()
        slideNote.text = note
        timeCode = TimeCodeParser.getTimeCodeFromNotes( slideNote )

        assert timeCode.totalTimeInSeconds == Settings.DEFAULT_SLIDE_TIME
        assert timeCode.lineNumber == 0
