import logging
import logging.config
import argparse
from timebuddy import Logger
from timebuddy.TimeBuddyApp import TimeBuddyApp


def init() :
    argParser = argparse.ArgumentParser( prog = 'timebuddy' , description = 'timebuddy - Empathischer Vortragsbegleiter' )
    argParser.add_argument( '--debug' , '-dbg' , action = 'store_true' , help = 'Enable debug mode' )

    args = argParser.parse_args()

    Logger.IsDebug = args.debug
    Logger.init()


# call this before any call/reference to Logger
if __name__ == "__main__" :
    init()


_logger = logging.getLogger( __name__ )


def main() :
    app = TimeBuddyApp()

# TODO:
# - save/load cfg
# - empathic calc
# - settings
# - overlays
# - capture times
# - tests
# - mockup presentation

if __name__ == "__main__" :
    main()

# pylint src --disable=C0103,C0116,W0107,C0114,C0115,R0903,C0301,W0511,W0613,C0304,C0305,R0902,W0613,W0611,C0303,W0612