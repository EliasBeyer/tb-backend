
from timebuddy.Configuration.Serializable import Serializable

# base class
class WindowConfiguration( Serializable ) :
    def __init__( self ) :
        self.width : int = 800
        self.height : int = 1000
        self.posX : int = 0
        self.posY : int = 0
        pass

    # override
    def loadFromDict( self , jsonDict : dict ) :
        self.width = int( jsonDict[ 'width' ] )
        self.height = int( jsonDict[ 'height' ] )
        self.posX = int( jsonDict[ 'posX' ] )
        self.posY = int( jsonDict[ 'posY' ] )
        pass

    # override
    def storeToDict( self ) -> dict :
        return {
            'width' : self.width ,
            'height' : self.height ,
            'posX' : self.posX ,
            'posY' : self.posY ,
        }

