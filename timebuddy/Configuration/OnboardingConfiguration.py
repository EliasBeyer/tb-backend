
from timebuddy.Configuration.Serializable import Serializable


class OnboardingConfiguration( Serializable ) :
    IDENTIFIER : str = "ONBOARDING"

    def __init__( self ) :

        pass

    # override
    def loadFromDict( self , jsonDict : dict ) :
        # self.urgency = float( jsonDict[ 'urgency' ] )
        pass

    # override
    def storeToDict( self ) -> dict :
        return {
            'urgency' : self.urgency ,
        }
