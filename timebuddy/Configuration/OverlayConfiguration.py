
# base
from timebuddy.Configuration.WindowConfiguration import WindowConfiguration
from timebuddy.Settings import Settings


class OverlayConfiguration( WindowConfiguration ) :
    IDENTIFIER : str = "OVERLAY"

    def __init__( self ) :
        super().__init__()
        # override defaults
        self.width = 800
        self.height = 400
        self.posX = 0
        self.posY = 0
        pass

    # override
    def loadFromDict( self , jsonDict : dict ) :
        super().loadFromDict( jsonDict )
        pass

    # override
    def storeToDict( self ) -> dict :
        return super().storeToDict() | {

        }


class OverlayConfiguration_Farbanzeige( OverlayConfiguration ) :
    IDENTIFIER : str = Settings.FARBANZEIGE_TITLE

    def __init__( self ) :
        super().__init__()
        self.colorTooFast : str = "hsl()"    # hsl - float hsl degrees?
        self.colorInTime : str = "hsl()"     # hsl - float hsl degrees?
        self.colorTooSlow : str = "hsl()"    # hsl - float hsl degrees?
        # TODO: should be in empathy
        self.urgency : float = 0 # in percent?
        self.tolerance : float = 0 # in percent?
        pass

    # override
    def loadFromDict( self , jsonDict : dict ) :
        super().loadFromDict( jsonDict )
        self.colorTooFast = jsonDict[ 'colorTooFast' ]
        self.colorInTime = jsonDict[ 'colorInTime' ]
        self.colorTooSlow = jsonDict[ 'colorTooSlow' ]
        self.urgency = float( jsonDict[ 'urgency' ] )
        self.tolerance = float( jsonDict[ 'tolerance' ] )
        pass

    # override
    def storeToDict( self ) -> dict :
        return super().storeToDict() | {
            'colorTooFast' : self.colorTooFast ,
            'colorInTime' : self.colorInTime ,
            'colorTooSlow' : self.colorTooSlow ,
            'urgency' : self.urgency ,
            'tolerance' : self.tolerance ,
        }


class OverlayConfiguration_Zeitbilanz( OverlayConfiguration ) :
    IDENTIFIER : str = Settings.ZEITBILANZ_TITLE

    def __init__( self ) :
        super().__init__()
        pass

    # override
    def loadFromDict( self , jsonDict : dict ) :
        super().loadFromDict( jsonDict )
        pass

    # override
    def storeToDict( self ) -> dict :
        return super().storeToDict() | {
            # nothing here yet
        }
