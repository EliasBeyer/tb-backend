
class Serializable :
    """
    Interface.

    Provides methods to load and store a class instance from and to a dictionary.
    """
    # abstract
    def loadFromDict( self , jsonDict : dict ) -> bool :
        """
        Abstract method (must be overridden)

        Updates and sets its member fields from a dictionary

        Parameters:
        - jsonDict (str): The dictionary returned by json.load()

        Returns:
        bool: Success - successfully loaded all fields
        """
        pass

    # abstract
    def storeToDict( self ) -> dict :
        """
        Abstract method (must be overridden)

        Loads a class member fields into a dictionary

        Returns:
        dict: The resulting dictionary containing the keys as string with the name of the field and as value the value of the field
        """
        return {}
