
import logging
from timebuddy.Configuration.Serializable import Serializable
from timebuddy.Configuration.EmpathyConfiguration import EmpathyConfiguration
from timebuddy.Configuration.OverlayConfiguration import OverlayConfiguration_Zeitbilanz, OverlayConfiguration_Farbanzeige


_logger = logging.getLogger( __name__ )


class ProfileConfiguration( Serializable ) :
    IDENTIFIER : str = "PROFILE"

    def __init__( self , isDefault : bool = False ) :
        self._isDefault : bool = isDefault # read only

        self.name : str = "<PROFILE>"
        self.description : str = "<DESCRIPTION>"
        self.smallsText : str = "<SMALLS-TEXT>"
        self.empathyConfiguration : EmpathyConfiguration = EmpathyConfiguration()
        # TODO: this as array, if more than 2 Overlays..
        self.overlayConfig_Zeitbilanz  : OverlayConfiguration_Zeitbilanz  = OverlayConfiguration_Zeitbilanz()
        self.overlayConfig_Farbanzeige : OverlayConfiguration_Farbanzeige = OverlayConfiguration_Farbanzeige()
        pass

    def isDefault( self ) -> bool :
        return self._isDefault

    # override
    def loadFromDict( self , jsonDict : dict ) :
        if self._isDefault :
            _logger.debug( 'Cannot load default profile from dict' )
            return

        self.name = jsonDict[ 'name' ]
        self.description = jsonDict[ 'description' ]
        self.smallsText = jsonDict[ 'smallsText' ]
        self.empathyConfiguration.loadFromDict( jsonDict[ EmpathyConfiguration.IDENTIFIER ] )
        self.overlayConfig_Zeitbilanz.loadFromDict( jsonDict[ OverlayConfiguration_Zeitbilanz.IDENTIFIER ] )
        self.overlayConfig_Farbanzeige.loadFromDict( jsonDict[ OverlayConfiguration_Farbanzeige.IDENTIFIER ] )
        pass

    # override
    def storeToDict( self ) -> dict :

        if self._isDefault :
            _logger.debug( 'Cannot store default profile from dict' )
            return {}

        return {
            'name' : self.name ,
            'description' : self.description ,
            'smallsText' : self.smallsText ,
            EmpathyConfiguration.IDENTIFIER : self.empathyConfiguration.storeToDict() ,
            OverlayConfiguration_Zeitbilanz.IDENTIFIER  : self.overlayConfig_Zeitbilanz.storeToDict()  ,
            OverlayConfiguration_Farbanzeige.IDENTIFIER : self.overlayConfig_Farbanzeige.storeToDict() ,
        }
