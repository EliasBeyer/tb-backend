
import json
import logging
import os
from timebuddy.Configuration.Serializable import Serializable
from timebuddy.Configuration.FrontendConfiguration import FrontendConfiguration
from timebuddy.Configuration.ProfileConfiguration import ProfileConfiguration
from timebuddy.Settings import Settings


# TODO: instead of jsonDict[ 'key' ] rewrite to use jsonDict.get( 'key' , default-value )

_logger = logging.getLogger( __name__ )


class Configuration( Serializable ) :
    def __init__( self ) :
        self.activeProfile : str = ""
        self.profiles : list[ ProfileConfiguration ] = self.getDefaultProfiles()
        self.frontendConfig : FrontendConfiguration = FrontendConfiguration()

        success = self.loadFromFile( Settings.CONFIG_FILENAME )

        assert success

        pass

    def loadFromFile( self , filename : str = Settings.CONFIG_FILENAME ) -> bool :
        """
        Loads the configuration from a file

        Parameters:
        - filename (str): The filename, defaults to 'cfg/timebuddy.json'.

        Returns:
        bool: Success - True when no errors occured, False otherwise.
        """
        if not os.path.exists( filename ) :
            _logger.info( f"Could not find {filename}" )
            _logger.info( f"Creating default {filename}..." )

            success = self.saveToFile( filename )

            if success :
                _logger.info( f"Done." )

            return success

        try :
            cfgStr = ""

            with open( filename , 'r' , encoding = 'utf-8' ) as file :
                cfgStr = file.read()

            cfgJson = json.loads( cfgStr )
            self.loadFromDict( cfgJson )

            return True

        except ( json.JSONDecodeError , KeyError ) as err :
            _logger.info( f"Corrupted {filename} - ignoring" )
            _logger.exception( err )
            return False
        except Exception as err :
            _logger.exception( err )
            return False

    def saveToFile( self , filename : str = Settings.CONFIG_FILENAME ) -> bool :

        try :
            cfgDict = self.storeToDict()
            cfgStr = json.dumps( cfgDict , indent = 4 )

            os.makedirs( os.path.dirname( filename ) , exist_ok = True )

            with open( filename , 'w' , encoding = 'utf-8' ) as file :
                file.write( cfgStr )

            return True

        except Exception as err :
            _logger.exception( err )
            return False

    # override
    def loadFromDict( self , jsonDict : dict ) :
        self.activeProfile = jsonDict.get( 'activeProfile' , Configuration.STANDARD_PROFILE_NAME )
        self.frontendConfig.loadFromDict( jsonDict[ FrontendConfiguration.IDENTIFIER ] )
        self.profiles = self.getDefaultProfiles()

        # load user profiles
        for jsonProfile in jsonDict[ ProfileConfiguration.IDENTIFIER ] :
            profile = ProfileConfiguration()
            profile.loadFromDict( jsonProfile )

            self.profiles.append( profile )

        pass

    # override
    def storeToDict( self ) -> dict :
        userProfiles = [ p for p in self.profiles if not p.isDefault() ]

        return {
            FrontendConfiguration.IDENTIFIER : self.frontendConfig.storeToDict() ,
            'activeProfile' : self.activeProfile ,
            ProfileConfiguration.IDENTIFIER  : [ p.storeToDict() for p in userProfiles ] ,
        }

    STANDARD_PROFILE_NAME = "Standard"

    def getDefaultProfiles( self ) -> list[ ProfileConfiguration ] :

        profile_Standard = ProfileConfiguration( isDefault = True )
        profile_Standard.name = Configuration.STANDARD_PROFILE_NAME
        profile_Standard.smallsText = "Wird immer beim Start der App geladen."
        profile_Standard.description = "Standard-Einstellungen für einen schnellen Start."
        profile_Standard.overlayConfig_Farbanzeige.tolerance = 0.1
        profile_Standard.overlayConfig_Farbanzeige.urgency = 0.1
        profile_Standard.overlayConfig_Farbanzeige.colorInTime = 0.1
        profile_Standard.overlayConfig_Farbanzeige.colorTooFast = 0.1
        profile_Standard.overlayConfig_Farbanzeige.colorTooSlow = 0.1

        profile_Vorlesung = ProfileConfiguration( isDefault = True )
        profile_Vorlesung.name = Configuration.STANDARD_PROFILE_NAME
        profile_Vorlesung.smallsText = "Wird immer beim Start der App geladen."
        profile_Vorlesung.description = "Standard-Einstellungen für einen schnellen Start."
        profile_Vorlesung.overlayConfig_Farbanzeige.tolerance = 0.1
        profile_Vorlesung.overlayConfig_Farbanzeige.tolerance = 0.1
        profile_Vorlesung.overlayConfig_Farbanzeige.urgency = 0.1
        profile_Vorlesung.overlayConfig_Farbanzeige.colorInTime = 0.1
        profile_Vorlesung.overlayConfig_Farbanzeige.colorTooFast = 0.1
        profile_Vorlesung.overlayConfig_Farbanzeige.colorTooSlow = 0.1

        profile_Verteidigung = ProfileConfiguration( isDefault = True )
        profile_Verteidigung.name = Configuration.STANDARD_PROFILE_NAME
        profile_Verteidigung.smallsText = "Wird immer beim Start der App geladen."
        profile_Verteidigung.description = "Standard-Einstellungen für einen schnellen Start."
        profile_Verteidigung.overlayConfig_Farbanzeige.tolerance = 0.1
        profile_Verteidigung.overlayConfig_Farbanzeige.tolerance = 0.1
        profile_Verteidigung.overlayConfig_Farbanzeige.urgency = 0.1
        profile_Verteidigung.overlayConfig_Farbanzeige.colorInTime = 0.1
        profile_Verteidigung.overlayConfig_Farbanzeige.colorTooFast = 0.1
        profile_Verteidigung.overlayConfig_Farbanzeige.colorTooSlow = 0.1

        profile_Vorstellung = ProfileConfiguration( isDefault = True )
        profile_Vorstellung.name = Configuration.STANDARD_PROFILE_NAME
        profile_Vorstellung.smallsText = "Wird immer beim Start der App geladen."
        profile_Vorstellung.description = "Standard-Einstellungen für einen schnellen Start."
        profile_Vorstellung.overlayConfig_Farbanzeige.tolerance = 0.1
        profile_Vorstellung.overlayConfig_Farbanzeige.tolerance = 0.1
        profile_Vorstellung.overlayConfig_Farbanzeige.urgency = 0.1
        profile_Vorstellung.overlayConfig_Farbanzeige.colorInTime = 0.1
        profile_Vorstellung.overlayConfig_Farbanzeige.colorTooFast = 0.1
        profile_Vorstellung.overlayConfig_Farbanzeige.colorTooSlow = 0.1

        profile_DryRun = ProfileConfiguration( isDefault = True )
        profile_DryRun.name = "Dry-Run"
        profile_DryRun.smallsText = ""
        profile_DryRun.description = ""
        profile_DryRun.overlayConfig_Farbanzeige.urgency = 0.0
        profile_DryRun.overlayConfig_Farbanzeige.tolerance = 1.0
        profile_DryRun.overlayConfig_Farbanzeige.tolerance = 0.1
        profile_DryRun.overlayConfig_Farbanzeige.urgency = 0.1
        profile_DryRun.overlayConfig_Farbanzeige.colorInTime = 0.1
        profile_DryRun.overlayConfig_Farbanzeige.colorTooFast = 0.1
        profile_DryRun.overlayConfig_Farbanzeige.colorTooSlow = 0.1

        return [
            profile_Standard ,
            profile_Vorlesung,
            profile_Verteidigung,
            profile_Vorstellung,
            profile_DryRun,
        ]
