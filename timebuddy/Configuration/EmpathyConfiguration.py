
from timebuddy.Configuration.Serializable import Serializable


class EmpathyConfiguration( Serializable ) :
    IDENTIFIER : str = "EMPATHY"

    def __init__( self ) :
        # TODO: these should be here..
        self.urgency : float = 0 # in percent?
        self.tolerance : float = 0 # in percent?
        pass

    # override
    def loadFromDict( self , jsonDict : dict ) :
        self.urgency = float( jsonDict[ 'urgency' ] )
        self.tolerance = float( jsonDict[ 'tolerance' ] )
        pass

    # override
    def storeToDict( self ) -> dict :
        return {
            'urgency' : self.urgency ,
            'tolerance' : self.tolerance ,
        }