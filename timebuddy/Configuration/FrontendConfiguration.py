
from timebuddy.Configuration.WindowConfiguration import WindowConfiguration


class FrontendConfiguration( WindowConfiguration ) :
    IDENTIFIER : str = "FRONTEND"

    def __init__( self ) :
        super().__init__()
        # override defaults
        self.width = 800
        self.height = 1000
        self.posX = 0
        self.posY = 0
        pass

    # override
    def loadFromDict( self , jsonDict : dict ) :
        super().loadFromDict( jsonDict )
        pass

    # override
    def storeToDict( self ) -> dict :
        return super().storeToDict() | {

        }
