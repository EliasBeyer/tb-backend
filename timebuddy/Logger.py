
# See: https://docs.python.org/3/howto/logging-cookbook.html

import os
import json
import logging
import logging.config
from logging import LogRecord
from datetime import datetime
from typing import Callable
from timebuddy.Settings import Settings


IsDebug = False

#
# ╭───────────────────────────────────────────────────────────────────────────╮
# │ Logger: initial setup                                                     │
# ╰───────────────────────────────────────────────────────────────────────────╯
#

def init() :

    os.makedirs( Settings.LOG_PATH , exist_ok = True )

    logLevel = logging.DEBUG if IsDebug else logging.WARNING

    frontendFormatter = FrontendFormatter()
    jsonFormatter = JsonFormatter()

    defaultLogFileHandler = logging.FileHandler( Settings.LOG_FILENAME_DEFAULT , mode = "w+" ) # 'w+' to replace file contents
    defaultLogFileHandler.setLevel( logLevel )
    defaultLogFileHandler.setFormatter( frontendFormatter )

    verboseFileHandler = logging.FileHandler( Settings.LOG_FILENAME_VERBOSE )
    verboseFileHandler.setLevel( logLevel )
    verboseFileHandler.setFormatter( jsonFormatter )

    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel( logLevel )
    consoleHandler.setFormatter( frontendFormatter )

    rootLogger = logging.getLogger()
    rootLogger.setLevel( logLevel )
    rootLogger.addHandler( defaultLogFileHandler )
    rootLogger.addHandler( verboseFileHandler )
    rootLogger.addHandler( consoleHandler )


#
# ╭──────────────────────────────────────────────────╮
# │ Formatter                                        │
# ╰──────────────────────────────────────────────────╯
#

class JsonFormatter( logging.Formatter ) :

    def __init__( self ) :
        super().__init__()

    def format( self , record : LogRecord ) -> str :
        recordDict : dict = {
                "category" : record.name.split( '.' )[ -1 ] , # filename as category?
                "message" : record.msg ,
                "level" : record.levelname ,
                "caller" : f"{record.filename}:{record.funcName}[{record.lineno}]" ,
            }

            # record.__dict__ :

            # "name": "timebuddy.TimeBuddyApp",
            # "msg": "onPresentationOpened: Data_Storage.pptx",
            # "args": [],
            # "levelname": "DEBUG",
            # "levelno": 10,
            # "pathname": "C:\\Uni\\Git\\timebuddy\\Refactor Backend\\timebuddy\\src\\timebuddy\\TimeBuddyApp.py",
            # "filename": "TimeBuddyApp.py",
            # "module": "TimeBuddyApp",
            # "exc_info": null,
            # "exc_text": null,
            # "stack_info": null,
            # "lineno": 241,
            # "funcName": "onPresentationOpened",
            # "created": 1702163704.0438433,
            # "msecs": 43.0,
            # "relativeCreated": 147479.26831245422,
            # "thread": 14380,
            # "threadName": "WindowsPptPresentationMonitor",
            # "processName": "MainProcess",
            # "process": 7000,

        return json.dumps( recordDict )


class FrontendFormatter( logging.Formatter ) :

    def __init__( self ) :
        super().__init__()

    def format( self , record : LogRecord ) -> str :
        return f"[{record.levelname}] ({record.name.split( '.' )[ -1 ]}) {record.msg}"

#
# ╭──────────────────────────────────────────────────╮
# │ Handler                                          │
# ╰──────────────────────────────────────────────────╯
#

class CallbackHandler( logging.Handler ) :

    def __init__( self , level = logging.DEBUG , callback : Callable = None ) :
        super().__init__( level )
        self.setFormatter( FrontendFormatter() ) # TODO: better one?
        self._callback = callback

    def emit( self , record : LogRecord ) :
        if self._callback is not None :
            self._callback( self.format( record ) )
