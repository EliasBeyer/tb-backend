
from datetime import datetime


class Settings :

    TITLE = "timebuddy"

    # should be a divisor of 1.0 or the timer looks janky in UI
    TIMER_DELAY : float = 0.5 # in seconds
    MONITOR_DELAY : float = 0.5 # time in seconds

    DEFAULT_SLIDE_TIME = 90

    TIME_PATTERN = '/time'

    LOG_PATH = "./logs"
    LOG_FILENAME_DEFAULT = f"{LOG_PATH}/timebuddy.log"
    LOG_FILENAME_VERBOSE = f"{LOG_PATH}/{datetime.now().strftime('%Y-%m-%d %H-%M-%S')}.log"

    CONFIG_FILENAME = "./cfg/timebuddy.json"

    # must match overlay-IDs in Javascript!
    NAME_FARBANZEIGE = "FARBANZEIGE"
    NAME_ZEITBILANZ = "ZEITBILANZ"

    FARBANZEIGE_TITLE = "Farbanzeige"
    FARBANZEIGE_URL = "./app-content-pages/Overlay-Farbanzeige.html"

    ZEITBILANZ_TITLE = "Zeitbilanz"
    ZEITBILANZ_URL = "./app-content-pages/Overlay-Zeitbilanz.html"

