
import logging
import threading
import time
import pythoncom
import win32com.client
import win32api
from timebuddy.Presentation.MicrosoftWindows.WindowsComPpt import WindowsComPpt
from timebuddy.Presentation.TimeCodeParser import TimeCodeParser
from timebuddy.Presentation.PresentationState import PresentationState
from timebuddy.Presentation.Slide import Slide
from timebuddy.Presentation.Presentation import Presentation
from timebuddy.Presentation.PresentationMonitor import PresentationMonitor
from timebuddy.Settings import Settings


_logger = logging.getLogger( __name__ )


class WindowsPptPresentationMonitor( PresentationMonitor ) :
    def __init__( self ) :
        super().__init__()

        self.pptApp : any = None
        self.comId : any = None
        self.pptIsOpen : bool = False

        self.monitorPptAppThread = threading.Thread( name = "WindowsPptPresentationMonitor" ,
                                                     daemon = True , # is this needed?
                                                     target = self.monitorPptApp )
        self.monitorPptAppThread.start()

        pass

    def setup( self ) :
        self.pptApp = WindowsComPpt.getApplication()

        # override events
        self.pptApp.OnPresentationOpen = self.onPresentationOpen
        self.pptApp.OnPresentationClose = self.onPresentationClose
        self.pptApp.OnSlideShowBegin = self.onSlideShowBegin
        self.pptApp.OnSlideShowEnd = self.onSlideShowEnd
        self.pptApp.OnSlideShowNextSlide = self.onSlideShowNextSlide

        # ??? is this needed and an actual ID?
        # self.comId = pythoncom.CoMarshalInterThreadInterfaceInStream( pythoncom.IID_IDispatch, self.pptApp )

        pass

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Monitor PPT state                                                        │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    def monitorPptApp( self ) :

        pythoncom.CoInitialize() # is this still needed?

        while True :
            isPresentationOpen = self.checkForOpenPresentation()

            if isPresentationOpen :
                pass

            time.sleep( Settings.MONITOR_DELAY )
        pass

    def checkForOpenPresentation( self ) -> bool :
            comApplication = WindowsComPpt.getApplication()

            if not comApplication :
                return False

            if not self.pptIsOpen : # needs rework, might fire twice
                # check for already opened presentations (events wont trigger, do it manually)
                self.setup()

                comPresentation = WindowsComPpt.getPresentation()

                if comPresentation is not None :
                    self.onPresentationOpen( comPresentation )
                    self.pptIsOpen = True

                    _logger.info( "Found new Presentation" )
                    pass
                pass

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ EVENTS (see: WindowsComPowerPointEvents)                                 │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    # TODO: check pres-ID and get my own Presentation-class instance instead of COM
    def onPresentationOpen( self , comPresentation ) :
        name = comPresentation.Name

        _logger.debug( f"handleOnPresentationOpen: {name}" )

        presentation = Presentation()
        presentation.name = name

        for comSlide in comPresentation.Slides :
            slide = Slide()
            slide.slideNumber = comSlide.SlideNumber
            slide.contents = ""
            slide.handle = comSlide
            slide.note = WindowsComPpt.getNotesFromSlide( comSlide )
            slide.timeCode = TimeCodeParser.getTimeCodeFromNotes( slide.note )
            slide.setTargetTime( slide.timeCode.totalTimeInSeconds )

            presentation.slides.append( slide )

        super().onPresentationOpened( presentation )
        pass

    def onPresentationClose( self , comPresentation ) :
        self.pptIsOpen = False
        self.presentationState = PresentationState.NoPresentation
        name = comPresentation.Name

        _logger.debug( f"handleOnPresentationClose: {name}" )

        super().onPresentationClosed( name )
        pass

    def onSlideShowBegin( self , comSlideViewWindow ) :
        self.presentationState = PresentationState.SlideShowActive

        name = comSlideViewWindow.Presentation.Name
        slideNumber = comSlideViewWindow.View.Slide.SlideNumber

        _logger.debug( f"handleOnSlideShowBegin: {name}" )

        super().onSlideShowBegin( name , slideNumber )
        pass

    def onSlideShowEnd( self , comSlideViewWindow ) :
        self.presentationState = PresentationState.PresentationOpen

        _logger.debug( f"handleOnSlideShowEnd" )

        super().onSlideShowEnd()
        pass

    def onSlideShowNextSlide( self , comSlideViewWindow ) :
        # when slide is changed end exites shortly after (guess its still in in animation),
        # slideViewWindow.View.Slide will be empty...
        try :
            slideNumber = comSlideViewWindow.View.Slide.SlideNumber

            _logger.debug( f"handleOnSlideShowNextSlide {slideNumber}" )

            # TODO: update self.pres with comSlideViewWindow.Presentation?

            super().onSlideShowNextSlide( self.presentation , slideNumber )
            pass
        except win32com.client.pywintypes.com_error as err :
            errMsg = win32api.FormatMessage( err.hresult )

            _logger.debug( f"handleOnSlideShowNextSlide - without active slide show! {errMsg}" )
            _logger.exception( err )

            pass

        pass

    # override
    def updateSlideNotesWithTimeDelta( self , slide : Slide ) -> bool : # success

        if not slide.handle :

            return

        slideTime = slide.getElapsedTime()

        if slideTime <= 0 :
            # dont update when no timer ran
            return True

        try :

            comPptPresentation = WindowsComPpt.getPresentation()
            comSlide = comPptPresentation.Slides.Item( slide.slideNumber ) # or slideIndex?

            slide.note.updateTextFromTimeCode( slide.timeCode )

            WindowsComPpt.writeToNotes( slide.note , comSlide )

            return True

        except win32com.client.pywintypes.com_error as err :
            errMsg = win32api.FormatMessage( err.hresult )
            _logger.err( f"updateSlideNotesWithTimeDelta {errMsg}" )
            _logger.exception( err )
            return False
        except Exception as err :
            _logger.error( f"Could not write elapsed time into notes!" )
            _logger.exception( err )
            return False
