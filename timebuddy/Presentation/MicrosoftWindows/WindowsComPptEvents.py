
class WindowsComPptEvents:
    """
    WindowsComPptEvents class defines event handlers for PowerPoint events.

    This class will be overridden, it is merely an interface.

    The names need to stay like so Windows-COM can recognize them.

    See: https://learn.microsoft.com/en-us/previous-versions/office/developer/office-2010/ff745350(v=office.14)#events

    Methods:
    - OnPresentationOpen( self , comPresentation ) :
        Called when a presentation is opened.

    - OnPresentationClose( self , comPresentation ) :
        Called when a presentation is closed.

    - OnSlideShowBegin( self , comSlideViewWindow ) :
        Called when a slideshow begins.

    - OnSlideShowEnd( self , comSlideViewWindow ) :
        Called when a slideshow ends.

    - OnSlideShowNextSlide( self , comSlideViewWindow ) :
        Called when the next slide is displayed in a slideshow.

    Parameters:
    - comPresentation: The COM object representing the PowerPoint presentation.
      See: https://learn.microsoft.com/en-us/previous-versions/office/developer/office-2010/ff746640(v=office.14)
    - comSlideViewWindow: The COM object representing the PowerPoint SlideViewWindow.
      See: https://learn.microsoft.com/en-us/previous-versions/office/developer/office-2010/ff744227(v=office.14)
    """

    def OnPresentationOpen(self, comPresentation):
        """
        Called when a presentation is opened.

        Parameters:
        - comPresentation: The COM object representing the PowerPoint presentation.
        """
        pass

    def OnPresentationClose(self, comPresentation):
        """
        Called when a presentation is closed.

        Parameters:
        - comPresentation: The COM object representing the PowerPoint presentation.
        """
        pass

    def OnSlideShowBegin(self, comSlideViewWindow):
        """
        Called when a slideshow begins.

        Parameters:
        - comSlideViewWindow: The COM object representing the PowerPoint SlideViewWindow.
        """
        pass

    def OnSlideShowEnd(self, comSlideViewWindow):
        """
        Called when a slideshow ends.

        Parameters:
        - comSlideViewWindow: The COM object representing the PowerPoint SlideViewWindow.
        """
        pass

    def OnSlideShowNextSlide(self, comSlideViewWindow):
        """
        Called when the next slide is displayed in a slideshow.

        Parameters:
        - comSlideViewWindow: The COM object representing the PowerPoint SlideViewWindow.
        """
        pass
