
import logging
import pythoncom
import win32com.client
import win32api
from timebuddy.Presentation.MicrosoftWindows.WindowsComPptEvents import WindowsComPptEvents
from timebuddy.Presentation.Slide import SlideNote


_logger = logging.getLogger( __name__ )


COM_APP_NAME = "PowerPoint.Application"


class WindowsComPpt :

    def getNotesFromSlide( comSlide ) -> SlideNote :

        slideNote = SlideNote()
        slideNote.text = comSlide.NotesPage.Shapes.Placeholders( 2 ).TextFrame.TextRange.Text

        return slideNote

    def writeToNotes( slideNote : SlideNote , comSlide : object ) :
        comSlide.NotesPage.Shapes.Placeholders( 2 ).TextFrame.TextRange.Text = slideNote.text

    def getApplication() -> object | None :
        pptApp = win32com.client.DispatchWithEvents( COM_APP_NAME , WindowsComPptEvents )

        return pptApp

    def getPresentation() -> object | None :
        try :
            # both of them seem to block PPT and make it unresponsive (combination with time.sleep())
            # dummy_pptApp = win32com.client.GetActiveObject( ComConstants.APP_NAME )
            # _pptApp = win32com.client.GetObject( COM_APP_NAME )
            pptApp = WindowsComPpt.getApplication()

            return pptApp.ActivePresentation

        except win32com.client.pywintypes.com_error as err : # expected error
            errMsg = win32api.FormatMessage( err.hresult )
            _logger.debug( f"checkForOpenPresentation {errMsg}" )
            _logger.exception( err )
            pass
        except TypeError as err : # expected error
            pass
        except AttributeError as err : # expected error
            pass
        except Exception as err : # unexpected error
            _logger.exception( f'monitorPptApp: {err}' )
            pass

        return None
