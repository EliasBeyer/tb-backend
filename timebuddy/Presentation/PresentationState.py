
# must match with JS
class PresentationState :
    NoPresentation = "NO_PRESENTATION" # no presentation opened
    PresentationOpen = "PRESENTATION_OPEN" # edit mode
    SlideShowActive = "ACTIVE_SLIDESHOW" # presenter mode, slideshow active
