
import logging
from timebuddy.Settings import Settings


_logger = logging.getLogger( __name__ )


class TimeCode :
    def __init__( self ) :
        self.lineNumber : int = 0
        self.delta : int = 0
        self.userMinutes : int = 0
        self.userSeconds : int = 0
        self.userTimeInSeconds : int = 0
        self.totalTimeInSeconds : int = 0

    def toString( self ) :
        text = f"{Settings.TIME_PATTERN}={self.userMinutes:2}:{self.userSeconds:2}"

        if self.delta != 0 :
            text += f"{self.delta}"

        return text

    def getDefault( timeInSeconds : int = Settings.DEFAULT_SLIDE_TIME ) :
        minutes , seconds = divmod( timeInSeconds , 60 )

        timeCode = TimeCode()
        timeCode.delta = 0
        timeCode.lineNumber = 0
        timeCode.userMinutes = minutes
        timeCode.userSeconds = seconds
        timeCode.userTimeInSeconds = timeInSeconds
        timeCode.totalTimeInSeconds = timeInSeconds

        return timeCode
