

import logging
import re
from timebuddy.Presentation.Slide import SlideNote
from timebuddy.Presentation.TimeCode import TimeCode
from timebuddy.Settings import Settings


_logger = logging.getLogger( __name__ )


class TimeCodeParser :

    def getTimeCodeFromNotes( slideNote : SlideNote ) -> TimeCode : # time in seconds

        if slideNote is None :
            return TimeCode.getDefault()

        if slideNote.text is None :
            return TimeCode.getDefault()

        foundPattern = False
        lineNumber = 0
        timeStr = ""
        pattern = Settings.TIME_PATTERN.lower()
        lines = slideNote.text.splitlines()
        lines = reversed( lines ) # last occourence overrides previous

        for idx , line in enumerate( lines ) :
            line = line.strip().lower()

            if line.startswith( pattern ) :
                foundPattern = True
                timeStr = line
                lineNumber = idx
                slideNote.timeCodeLineNumber = idx
                break

            pass

        if not foundPattern :
            return TimeCode.getDefault()

        # Explanation of the regex pattern:
        # /time           - Matches the literal string "/time"
        # \s*             - Matches zero or more whitespace characters
        # =               - Matches the literal string "="
        # \s*             - Matches zero or more whitespace characters
        # (?P<minutes>    - Named capturing group for minutes
        #   \d{1,2})      - Matches exactly 1 or 2 digits representing minutes
        # :               - Matches the literal colon character ":"
        # (?P<seconds>    - Named capturing group for seconds
        #   \d{1,2})      - Matches exactly 1 or 2 digits representing seconds
        # (?P<delta>      - Named capturing group for delta (optional)
        #   [+-]\d+)?     - Matches a mandatory sign (+ or -) followed by one or more digits.
        #                 - The entire delta part is enclosed in parentheses and marked as optional with the "?".
        PATTERN = r'/time\s*=\s*(?P<minutes>\d{1,2}):(?P<seconds>\d{1,2})(?P<delta>[+-]\d+)?'

        rePattern = re.compile( PATTERN , re.IGNORECASE )
        match = re.match( rePattern , timeStr )

        if not match:
            _logger.warning( f"Could not parse time command: {timeStr}" )
            return TimeCode.getDefault()

        timeCode = TimeCode()
        timeCode.lineNumber  = lineNumber
        timeCode.userMinutes = int( match.group( 'minutes' ) )
        timeCode.userSeconds = int( match.group( 'seconds' ) )
        timeCode.delta       = int( match.group( 'delta' ) ) if match.group( 'delta' ) else 0
        timeCode.userTimeInSeconds  = timeCode.userMinutes * 60 + timeCode.userSeconds
        timeCode.totalTimeInSeconds = timeCode.userTimeInSeconds + timeCode.delta

        return timeCode

