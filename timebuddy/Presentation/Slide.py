
import logging
import time
from timebuddy.Presentation.SlideNote import SlideNote
from timebuddy.Presentation.TimeCode import TimeCode
from timebuddy.Settings import Settings


_logger = logging.getLogger( __name__ )


class Slide :
    def __init__( self ) :
        self.slideNumber : int = -1 # should start at 0 for easy array access

        self.note : SlideNote = SlideNote()
        self.timeCode : TimeCode = TimeCode.getDefault()

        self.notesTime : int  = 0 # explicit given time (notes/external resource)
        self.noteContents : str = "" #

        self.contents : str = "" # maybe used for approx time

        self.averageTime : int = 0 # from previous runs
        self.averageCount : int = 0 # number of prev runs
        self.currentTime : int = 0 # Timer is running
        self.calculatedTime : int = 0 # calculated
        self.optimalTime : int = 0

        self.handle : any = None # backlink to original slide

        self.startTime : int = 0
        self.elapsedTime : int = 0

        pass

    def resetTime( self ) :
        self.elapsedTime = 0
        self.startTime = 0

    def startTimer( self ) :
        # already started, so do nothing
        if self.startTime != 0 :
            return

        self.startTime = time.time()

        pass

    def stopTimer( self ) :
        # not yet started, so do nothing
        if self.startTime == 0 :
            return

        self.elapsedTime += time.time() - self.startTime
        self.startTime = 0 # so we can track the next time part
        pass

    def getElapsedTime( self ) :
        # if stopped
        if self.startTime == 0 :
            return self.elapsedTime
        else : # running - calculate current elapsed time
            return self.elapsedTime + ( time.time() - self.startTime )

    def setTargetTime( self , targetTime : int ) :
        if targetTime <= 0 :
            targetTime = Settings.DEFAULT_SLIDE_TIME

        self.notesTime = targetTime

        pass

    pass
