
from timebuddy.Presentation.Presentation import Presentation, PresentationCallbacks
from timebuddy.Presentation.PresentationState import PresentationState
from timebuddy.Presentation.Slide import Slide


class PresentationMonitor :

    def __init__( self ) :
        self.presentationState : PresentationState = PresentationState.NoPresentation
        self.presentation : Presentation = None
        self.callbacks : PresentationCallbacks = PresentationCallbacks()

        self.currentSlide : Slide = None # reference to presentation.slides[ currentSlideIndex ]
        self.currentSlideIndex : int = -1 # index in presentation.slides-array, starts at 0
        self.currentSlideNumber : int = 0 # given by presentation, might start at 1

        self.previousSlide : Slide = None # reference to presentation.slides[ previousSlideIndex ]
        self.previousSlideIndex : int = -1 # index in presentation.slides-array, starts at 0
        self.previousSlideNumber : int = 0 # given by presentation, might start at 1
        pass

    # >>> EVENTS

    def onPresentationOpened( self , presentation : Presentation ) :
        # TODO: handle multiple open presentations
        # dont override if presentation already exists
        # if self.presentation is None :
            # self.presentation = presentation

        self.presentation = presentation # HACK: current pres gets overridden

        self.presentationState = PresentationState.PresentationOpen

        if self.callbacks.onPresentationOpened :
            self.callbacks.onPresentationOpened( presentation )

        pass

    def onPresentationClosed( self , presentationName : str ) :
        if self.callbacks.onPresentationClosed :
            self.callbacks.onPresentationClosed( presentationName )

        # HACK: hadnel this properly
        # self.presentation = None
        self.presentationState = PresentationState.NoPresentation # set state after callback

        pass

    def onSlideShowBegin( self , presentationName : str , slideNumber : int ) :
        self.presentationState = PresentationState.SlideShowActive

        if self.callbacks.onSlideShowBegin :
            self.callbacks.onSlideShowBegin( presentationName )
        pass

    def onSlideShowEnd( self ) :
        self.presentationState = PresentationState.PresentationOpen

        if self.callbacks.onSlideShowEnd :
            self.callbacks.onSlideShowEnd()
        pass

    def onSlideShowNextSlide( self , presentation : Presentation , slideNumber : int ) :
        assert self.presentation == presentation
        assert slideNumber >= 0

        # update references to prev slide
        self.previousSlide = self.currentSlide
        self.previousSlideIndex = self.currentSlideIndex
        self.previousSlideNumber = self.currentSlideNumber

        if self.previousSlide :
            self.previousSlide.stopTimer()

        # get new slide
        slidesWithSlideNumber = ( ( idx , s ) for idx , s in enumerate( presentation.slides ) if s.slideNumber == slideNumber ) # should be only 1
        idx , slide = next( slidesWithSlideNumber , ( -1 , None ) ) # get the first one (or None)

        self.currentSlide = slide
        self.currentSlideIndex = idx
        self.currentSlideNumber = slideNumber

        # TODO: better error handling
        assert slide
        assert idx >= 0

        if self.callbacks.onSlideShowNextSlide :
            self.callbacks.onSlideShowNextSlide( self.currentSlide )

        pass

    # <<< EVENTS

    # abstract
    def updateSlideNotesWithTimeDelta( self , slide : Slide ) -> bool :
        pass
