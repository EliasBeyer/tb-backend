
import logging
from timebuddy.Presentation.TimeCode import TimeCode


_logger = logging.getLogger( __name__ )


class SlideNote :
    def __init__( self ) :
        self.text : str
        self.timeCodeLineNumber : int = 0

    def updateTextFromTimeCode( self , timeCode : TimeCode ) :
        lines = self.text.splitlines()
        lines[ self.timeCodeLineNumber ] = timeCode.toString()

        self.text = '\n'.join( lines )

