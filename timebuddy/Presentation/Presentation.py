
from typing import Callable
from timebuddy.Presentation.Slide import Slide


class Presentation :
    def __init__( self , name : str = "<PRESENTATION>" ) :
        self.name : str = name
        self.slides : list[Slide] = []
        pass


class PresentationCallbacks :
    def __init__( self ) :
        self.onPresentationOpened : Callable = None
        self.onPresentationClosed : Callable = None
        self.onSlideShowBegin : Callable = None
        self.onSlideShowEnd : Callable = None
        self.onSlideShowNextSlide : Callable = None
        pass



