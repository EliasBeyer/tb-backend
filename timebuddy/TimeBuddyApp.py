
import logging
import time
import threading
import pythoncom
from timebuddy.Presentation.Slide import Slide
from timebuddy.Settings import Settings
from timebuddy.Timer import Timer, TimerState
from timebuddy.Presentation.PresentationState import PresentationState
from timebuddy.Logger import CallbackHandler
from timebuddy.Configuration.Configuration import Configuration
from timebuddy.Presentation.Presentation import Presentation
from timebuddy.Presentation.PresentationMonitor import PresentationMonitor
from timebuddy.Presentation.MicrosoftWindows.WindowsPptPresentationMonitor import WindowsPptPresentationMonitor
from timebuddy.WebviewWindows.OverlayWindow import OverlayWindow
from timebuddy.WebviewWindows.FrontendWindow import FrontendWindow
from timebuddy.WebviewWindows.WebviewWindow import displayWindows_blocking


_logger = logging.getLogger( __name__ )


#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Main App                                                                 │
# ╰──────────────────────────────────────────────────────────────────────────╯
#


class TimeBuddyApp :
    """ App Klasse
    """
    def __init__( self ) :
        # all member fields
        self._frontendWindow : FrontendWindow = None
        self._overlayWindow : OverlayWindow = None # currently only 1 overlay
        self._presentation : Presentation = None
        self._presentationMonitor : PresentationMonitor = None
        self._timer : Timer = None
        self._config : Configuration = None
        self._monitorTimerThread : threading.Thread = None

        self._frontendTextareaHandler : CallbackHandler = None
        self._frontendPopupHandler : CallbackHandler = None

        # TODO: debug/release mode?

        self._init()

        displayWindows_blocking( self._continuationFunc )

        pass

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Private Methods                                                          │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    def _init( self ) :
        self._loadConfiguration() # load settings first (to start windows on dim/pos)

        self._initFrontend() # load frontend early for Logger
        self._initPresentation()
        self._initOverlay()
        self._initTimer()
        pass

    def _loadConfiguration( self ) :
        self._config = Configuration()
        pass

    def _initFrontend( self ) :
        self._frontendWindow : FrontendWindow = FrontendWindow()

        # get all events from the Frontend UI into the main App

        self._frontendWindow.callbacks.onWindowClosed  = self._onFrontendWindowClosed
        self._frontendWindow.callbacks.onWindowClosing = self._onFrontendWindowClosing
        self._frontendWindow.callbacks.onWindowMoved   = self._onFrontendWindowMoved
        self._frontendWindow.callbacks.onWindowResized = self._onFrontendWindowResized
        self._frontendWindow.callbacks.onWindowLoaded  = self._onFrontendWindowLoaded

        self._frontendWindow.webViewApi.callbacks.onBtnResetTimerClicked  = self._onBtnResetTimerClicked
        self._frontendWindow.webViewApi.callbacks.onBtnToggleTimerClicked = self._onBtnToggleTimerClicked
        self._frontendWindow.webViewApi.callbacks.onConfigChanged         = self._onConfigChanged

        self._frontendWindow.createWindow()

        pass

    def _initPresentation( self ) :
        useMockupPresentation = False

        if useMockupPresentation : # debug, not yet implemented
            pass
        else : # default windows ppt
            self._presentationMonitor = WindowsPptPresentationMonitor()
            self._presentationMonitor.callbacks.onPresentationOpened = self._onPresentationOpened
            self._presentationMonitor.callbacks.onPresentationClosed = self._onPresentationClosed
            self._presentationMonitor.callbacks.onSlideShowBegin     = self._onSlideShowBegin
            self._presentationMonitor.callbacks.onSlideShowEnd       = self._onSlideShowEnd
            self._presentationMonitor.callbacks.onSlideShowNextSlide = self._onSlideShowNextSlide
        pass

    def _initOverlay( self ) :

        pass

    def _initTimer( self ) :
        self._timer = Timer()
        self._startTimerMonitor()
        pass

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ After Windows Setup                                                      │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    # gets called after webview.start() in a new thread
    def _continuationFunc( self ) :
        try:
            _logger.info( "Load frontend window pos/dim" )

            self._frontendWindow.windowHandle.resize( self._config.frontendConfig.width , self._config.frontendConfig.height )
            self._frontendWindow.windowHandle.move(   self._config.frontendConfig.posX  , self._config.frontendConfig.posY   )

        except Exception as err :
            # since started in separate thread, wrap in try/except to be sure
            _logger.error( err )
            pass


#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Timer                                                                    │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    def _startTimerMonitor( self ) :
        _logger.info( "Start Timer monitor thread" )

        self._monitorTimerThread = threading.Thread( name = "monitorTimerThread" ,
                                                     daemon = True , # is this needed?
                                                     target = self._monitorTimer )
        self._monitorTimerThread.start()

        pass

    def _monitorTimer( self ) :
        try:
            pythoncom.CoInitialize() # is this still needed?

            # TIMER_DELAY must divide 1.0 evenly
            # means, timer is called exactly X times a second
            # otherwise the timer in UI will look stuttery
            # as it changes not exactly every second
            # TODO: make sure the math checks out
            # TODO: make sure pythons comparison float == float checks out
            # TODO: this check more in TimerConstants?
            _dbg_dividesEvenly = float( int( 1.0 / Settings.TIMER_DELAY ) ) == ( 1.0 / Settings.TIMER_DELAY )
            assert _dbg_dividesEvenly , f'TIMER_DELAY must divide 1.0 evenly'

            while True :

                time.sleep( Settings.TIMER_DELAY )

                elapsedSecs = self._timer.getElapsedSeconds()
                formattedTime = self._timer.getFormattedTime()
                jsStr = f'setTimer( {elapsedSecs} , "{formattedTime}" )'

                self._frontendWindow.evaluateJs( jsStr )
        except Exception as err :
            _logger.exception( err )
            pass
        pass


#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Logger                                                                   │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    def _linkLoggerToFrontend( self ) :
        """Links the root-logger output via CallbackHandler's to the Frontend (Popups and Textarea)
        """

        assert self._frontendWindow
        assert self._frontendWindow.isLoaded , "The webview-window must be loaded first!"

        _logger.debug( "Linking logger to Frontend..." )

        # TODO: levels should come from config
        self._frontendPopupHandler    = CallbackHandler( level = logging.CRITICAL , callback = self._frontendWindow.logToPopup    )
        self._frontendTextareaHandler = CallbackHandler( level = logging.INFO     , callback = self._frontendWindow.logToTextarea )

        _rootLogger = logging.getLogger()
        _rootLogger.addHandler( self._frontendPopupHandler )
        _rootLogger.addHandler( self._frontendTextareaHandler )

        _logger.debug( "Logger linked to Frontend." )

        pass

    def _unlinkLoggerFromFrontend( self ) :
        """Unlinks the root-logger output from the Frontend. Must be linked before.
        """
        if not self._frontendPopupHandler or not self._frontendTextareaHandler :
            _logger.debug( "_unlinkLoggerFromFrontend() has been called without calling _linkLoggerToFrontend() before" )
            raise RuntimeError()

        rootLogger = logging.getLogger()
        rootLogger.removeHandler( self._frontendPopupHandler )
        rootLogger.removeHandler( self._frontendTextareaHandler )

        self._frontendPopupHandler    = None
        self._frontendTextareaHandler = None

        _logger.debug( "Unlinked logger from Frontend." )

        pass

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Webview Callbacks                                                        │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Window Callbacks                                                         │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    def _onFrontendWindowLoaded( self ) :
        _logger.info( f"Loaded!" )

        # link logger after the window and JS has fully loaded
        # otherwise it will throw errors
        # as evaluateJs cant find the window JS runtime)
        self._linkLoggerToFrontend()
        pass

    def _onFrontendWindowClosed( self ) :
        _logger.info( f"Closing..." )

        pass

    def _onFrontendWindowResized( self , width : int , height : int ) :
        _logger.info( f"Window resized to {width}x{height}" )

        self._updateFrontendWindowConfig()
        pass

    def _onFrontendWindowMoved( self , posX : int , posY : int ) :
        _logger.info( f"Window moved to {posX}x{posY}" )

        self._updateFrontendWindowConfig()
        pass

    def _onFrontendWindowClosing( self ) :

        self._unlinkLoggerFromFrontend()

        if not self._frontendWindow.isLoaded :
            return

        _logger.info( f"Window closing..." )

        try :
            self._updateFrontendWindowConfig()
            self._config.saveToFile()
        except Exception as err :
            _logger.exception( err )
            pass
        pass

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ JS API Callbacks                                                         │
# ╰──────────────────────────────────────────────────────────────────────────╯
#
    def _onBtnToggleTimerClicked( self ) -> TimerState :
        self._timer.toggle()
        newState = self._timer.getState()

        match newState :

            case TimerState.Paused :
                if self._presentationMonitor.currentSlide :
                    self._presentationMonitor.currentSlide.stopTimer()
                pass
            case TimerState.Running :
                if self._presentationMonitor.currentSlide :
                    self._presentationMonitor.currentSlide.startTimer()
                pass

            case _ :
                _logger.error( f'Impossible Timer State of: {newState}' )
        pass

        # TODO: send to overlays
        # if self._overlayWindow :
            # self._overlayWindow.evaluateJs( )

        _logger.info( f"Timer toggled to state: {newState}" )

        self._updateFrontendTitle()

        return newState

    def _onBtnResetTimerClicked( self ) -> TimerState :
        self._timer.reset()
        newState = self._timer.getState()

        assert newState == TimerState.Paused

        # TODO: send to overlays
        # if self._overlayWindow :
            # self._overlayWindow.evaluateJs( )

        # TODO: reset all slides or save

        self._updateFrontendTitle()

        _logger.info( f"Timer Reset to state: {newState}" )

        if self._presentation :
            # TODO wait for confirmation elsewhere JS
            for slide in self._presentation.slides :
                self._presentationMonitor.updateSlideNotesWithTimeDelta( slide )

        return newState

    def _onConfigChanged( self , category : str , key : str , value : str ) :
        _logger.info( f"Configuration changed: [{key}] = {value}" )

        # TODO: send to Configuration + validate!
        pass

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Presentation Events                                                      │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    def _onPresentationOpened( self , presentation : Presentation ) :
        _logger.info( f"A Presentation has been opened: {presentation.name}" )

        self._presentation = presentation

        self._onUpdateState()

        pass

    def _onPresentationClosed( self , presentationName : str ) :
        _logger.info( f"A Presentation has been closed: {presentationName}" )

        self._onUpdateState()

        pass

    def _onSlideShowBegin( self , presentationName : str ) : # TODO trigger for current slide?
        _logger.info( f"A Slideshow has been started: {presentationName}" )

        self._onUpdateState()

        pass

    def _onSlideShowEnd( self ) :
        _logger.info( f"A Slideshow has been ended" )

        self._onUpdateState()

        pass

    def _onSlideShowNextSlide( self , slide : Slide ) :
        _logger.info( f"A new slide has been changed: {self._presentation.name} - #{slide.slideNumber}" )

        if self._timer.getState() == TimerState.Running :

            if self._presentationMonitor.currentSlide :
                self._presentationMonitor.currentSlide.startTimer()
            if self._presentationMonitor.previousSlide :
                self._presentationMonitor.previousSlide.stopTimer()

        pass

#
# ╭──────────────────────────────────────────────────────────────────────────╮
# │ Logic & Utility                                                          │
# ╰──────────────────────────────────────────────────────────────────────────╯
#

    def _onUpdateState( self ) :
        self._updateFrontendTitle()

        state = self._presentationMonitor.presentationState

        self._frontendWindow.evaluateJs( f'changePresentationState( {state} )')

        pass

    def _updateFrontendTitle( self ) :
        state = self._presentationMonitor.presentationState

        if state == PresentationState.NoPresentation :
            self._frontendWindow.setTitle( state )
        else :
            assert self._presentation is not None

            self._frontendWindow.setTitle( state + ' | ' + self._presentation.name )
        pass

    def _updateFrontendWindowConfig( self ) :

        width  = self._frontendWindow.windowHandle.width
        height = self._frontendWindow.windowHandle.height
        posX   = self._frontendWindow.windowHandle.x
        posY   = self._frontendWindow.windowHandle.y

        self._config.frontendConfig.width  = width
        self._config.frontendConfig.height = height
        self._config.frontendConfig.posX   = posX
        self._config.frontendConfig.posY   = posY

        pass
