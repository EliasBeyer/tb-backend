
from timebuddy.WebviewWindows.Overlay import Overlay
from timebuddy.WebviewWindows.OverlayApi import OverlayApi
from timebuddy.WebviewWindows.WebviewWindow import WebViewWindow


class OverlayWindow( WebViewWindow ) :

    def __init__( self , overlay : Overlay ) :
        super().__init__()
        self.webViewApi = overlay.api
        self.overlay : Overlay = overlay

        self.title = self.overlay.title
        self.url = self.overlay.url
        pass

    def createWindow( self ) :

        width = 800
        height = 800
        posX = 0
        posY = 0

        super().createWindow( width , height , posX , posY )

    pass