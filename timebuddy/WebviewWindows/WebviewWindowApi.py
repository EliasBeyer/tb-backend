
from timebuddy.WebviewWindows.ApiCallbacks import WebviewApiCallbacks


class WebviewWindowApi :

    def __init__( self ) :
        self.callbacks : WebviewApiCallbacks = WebviewApiCallbacks()
        pass

    def onBtnToggleTimerClicked( self ) -> any :
        if self.callbacks.onBtnToggleTimerClicked :
            return self.callbacks.onBtnToggleTimerClicked()
        return None

    def onBtnResetTimerClicked( self ) -> any :
        if self.callbacks.onBtnResetTimerClicked :
            return self.callbacks.onBtnResetTimerClicked()
        return None

    pass