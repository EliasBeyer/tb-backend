
from timebuddy.Settings import Settings
from timebuddy.WebviewWindows.OverlayApi import OverlayApi_Farbanzeige, OverlayApi_Zeitbilanz


class Overlay :
    def __init__( self , title : str , url : str ) :
        self.title : str = "OVERLAY"
        self.url : str = ""
        self.settings : dict[ str , any ] = {}
        pass
    pass


class Overlay_Farbanzeige( Overlay ) :
    def __init__( self ) :
        super().__init__( title = Settings.FARBANZEIGE_TITLE , url = Settings.FARBANZEIGE_URL )
        self.api = OverlayApi_Farbanzeige()
    pass


class Overlay_Zeitbilanz( Overlay ) :
    def __init__( self ) :
        super().__init__( title = Settings.ZEITBILANZ_TITLE , url = Settings.ZEITBILANZ_URL )
        self.api = OverlayApi_Zeitbilanz()

    pass

