import logging
from typing import Callable
import webview

from timebuddy.WebviewWindows.WebviewWindowApi import WebviewWindowApi
from timebuddy.WebviewWindows.WebviewWindowCallbacks import WebViewWindowCallbacks


_logger = logging.getLogger( __name__ )


# disable logging for webview module
# logging.getLogger( "pywebview" ).setLevel( logging.WARNING )
# logging.getLogger( "pywebview" ).propagate = False


def displayWindows_blocking( continuationFunc : Callable ) -> object:
    """
    Blockiert den Thread und startet die Webansicht.
    Muss zuletzt aufgerufen werden.

    :return continuationFunc : Die Funktion, die nach dem Start der Webansicht ausgeführt wird.
    """
    assert continuationFunc

    webview.start( continuationFunc
                 , http_server = True # use webview builtin webserver
                 , debug = True # Browser-Debugger nicht in Release-Builds
                 )

    pass


class WebViewWindow :
    """
    Klasse zur Erstellung und Verwaltung eines WebView-Fensters.
    """

    def __init__( self ) :
        self.webViewApi : WebviewWindowApi = WebviewWindowApi()
        self.callbacks : WebViewWindowCallbacks = WebViewWindowCallbacks()
        self.title : str = ""
        self.url : str = ""
        self.drawOnTop : bool = False
        self.isClosing : bool = False
        self.isLoaded : bool = False
        self.windowHandle : any = None

        pass


    def createWindow( self , width : int , height : int , posX : int , posY : int ) :

        assert self.windowHandle is None , "Call self.windowHandle.destroy() before creating a new window!"

        self.windowHandle = webview.create_window( title = self.title
                                                 , url = self.url
                                                 , on_top = self.drawOnTop
                                                 , js_api = self.webViewApi
                                                 , width = width
                                                 , height = height
                                                 , x = posX
                                                 , y = posY
                                                 )

        assert self.windowHandle

        # See: https://pywebview.flowrl.com/examples/events.html
        self.windowHandle.events.loaded += self.onWindowLoaded
        self.windowHandle.events.shown += self.onWindowShown
        self.windowHandle.events.closing += self.onWindowClosing
        self.windowHandle.events.closed += self.onWindowClosed
        self.windowHandle.events.moved += self.onWindowMoved
        self.windowHandle.events.resized += self.onWindowResized

        pass


    def onWindowLoaded( self ):
        self.isLoaded = True

        if self.callbacks.onWindowLoaded :
            self.callbacks.onWindowLoaded()

        pass

    def onWindowShown( self ) :
        self.isLoaded = True # ?
        pass

    def destroyWindow( self ) :
        self.windowHandle.destroy()

        self.windowHandle is None
        pass

    def onWindowClosed( self ) :

        # update State (overlay/frontend)
        # destroy ovelays when frontend closed
        # update pos/dim of window

        pass

    def onWindowClosing( self ) :
        self.isClosing = True

        if self.callbacks.onWindowClosing and self.isLoaded :
            self.callbacks.onWindowClosing()

        pass

    def onWindowResized( self , width : int , height : int ) :
        if self.callbacks.onWindowResized and self.isLoaded :
            self.callbacks.onWindowResized( width , height )
        pass

    def onWindowMoved( self , posX : int , posY : int ) :
        if self.callbacks.onWindowMoved and self.isLoaded :
            self.callbacks.onWindowMoved( posX , posY )
        pass

    def checkJsAvailability( self ) :
        # When closing the window too fast,
        # or window is already closing,
        # JS is not loaded, so skip this
        if not self.isLoaded :
            _logger.warning( "Window not yet fully initialized, JS not yet available!" )
            return False
        elif self.isClosing : # throws KeyError when closing window + eval js
            _logger.warning( "Closed window already, JS is no longer available!" )
            return False

        return True

    def evaluateJs( self , jsScript : str ) :

        if not self.checkJsAvailability() :
            return

        try :
            _logger.debug( "Evaluate JS: " + jsScript )

            self.windowHandle.evaluate_js( jsScript )
        except KeyError as err : # window is closing
            _logger.exception( err )
            pass
        except webview.WebViewException as err : # window closed too fast or quit during startup
            _logger.exception( err )
            pass
        except Exception as err : # unknown exception
            _logger.exception( err )
            pass
        pass

    def setTitle( self , title : str ) :
        assert title
        self.windowHandle.set_title( title )
        pass
