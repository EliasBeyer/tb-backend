
from typing import Callable


# all callbacks in a class for easy passing from webview-api object -> window -> timebuddy-app

class WebviewApiCallbacks :
    def __init__( self ) :
        self.onBtnToggleTimerClicked : Callable = None # used in both: frontend & overlay
        self.onBtnResetTimerClicked  : Callable = None # used in both: frontend & overlay
        pass


class FrontendApiCallbacks( WebviewApiCallbacks ) :
    def __init__( self ) :
        super().__init__()
        self.onConfigChanged : Callable = None
        pass


class OverlayApiCallbacks( WebviewApiCallbacks ) :
    def __init__( self ) :
        super().__init__()
        pass


class OverlayApiCallbacks_Farbanzeige( OverlayApiCallbacks ) :
    def __init__( self ) :
        super().__init__()
        pass


class OverlayApiCallbacks_Zeitbilanz( OverlayApiCallbacks ) :
    def __init__( self ) :
        super().__init__()
        pass