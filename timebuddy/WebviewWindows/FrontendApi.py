
from timebuddy.WebviewWindows.ApiCallbacks import FrontendApiCallbacks
from timebuddy.WebviewWindows.WebviewWindowApi import WebviewWindowApi


class FrontendApi( WebviewWindowApi ) :

    def __init__( self ) :
        super().__init__()
        self.callbacks : FrontendApiCallbacks = FrontendApiCallbacks()
        pass

    # >>> Webview Callbacks

    # category: 'empathy', 'overlay-farbanzeige' ... -> create enum for this or find better way
    # key: 'colorTooFast'
    # value: int/float/str depending on key, parsed in TimeBuddyApp
    # validation also in TimeBuddyApp / Configuration.py I guess
    def onConfigChanged( self , category : str , key : str , value : str ) -> any :
        if self.callbacks.onConfigChanged :
            return self.callbacks.onConfigChanged( category , key , value )
        return None

    # <<< Webview Callbacks

    pass
