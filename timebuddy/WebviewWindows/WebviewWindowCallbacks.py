from typing import Callable


class WebViewWindowCallbacks :
    def __init__( self ) :
        self.onWindowClosing : Callable = None
        self.onWindowClosed : Callable = None
        self.onWindowMoved : Callable = None
        self.onWindowResized : Callable = None
        self.onWindowLoaded : Callable = None
        pass
