
import logging
from timebuddy.WebviewWindows.FrontendApi import FrontendApi
from timebuddy.WebviewWindows.WebviewWindow import WebViewWindow


_logger = logging.getLogger( __name__ )


FRONTEND_TITLE = "timebuddy"
FRONTEND_URL = "./app-content-pages/Frontend.html"


class FrontendWindow( WebViewWindow ) :

    def __init__( self ) :
        super().__init__()
        self.title = FRONTEND_TITLE
        self.url = FRONTEND_URL
        self.drawOnTop = False
        self.webViewApi : FrontendApi = FrontendApi()
        pass

    def createWindow( self ) :

        # TODO: get best pos/dim for window or from json
        width = 800
        height = 800
        posX = 0
        posY = 0

        super().createWindow( width , height , posX , posY )


    def logToTextarea( self , msg : str ) :
        self.evaluateJs( f"logToTextarea( '{msg}' )" )

    def logToPopup( self , msg : str ) :
        self.evaluateJs( f"logToPopup( '{msg}' )" )

    pass

