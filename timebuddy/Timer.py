# pylint src --disable=C0103,C0116,W0107,C0114,C0115,R0903,C0301,W0511,W0613,C0304,C0305,R0902,W0613,W0611,C0303,W0612


import logging
import time
from timebuddy.TimerState import TimerState


_logger = logging.getLogger( __name__ )


class Timer :

    def __init__( self ) :
        self.startTime : float = 0
        self.elapsedTime : float = 0 # time in seconds
        self.state : TimerState = TimerState.Paused
        self.reset()
        pass

    def getState( self ) -> TimerState :
        return self.state

    def start( self ) :
        self.state = TimerState.Running
        self.startTime = time.time() # time in secs since epoch
        pass

    def stop( self ) :
        self.state = TimerState.Paused
        self.elapsedTime += time.time() - self.startTime
        pass

    def toggle( self ) :

        match self.state :
            case TimerState.Paused :
                self.start()
                pass
            case TimerState.Running :
                self.stop()
                pass
            case _ : # should never happen
                _logger.error( f'Unexpected TimerState: {self.state}' )
                pass

        return self.state

    def reset( self ) : # stops timer
        self.state = TimerState.Paused
        self.startTime = 0
        self.elapsedTime = 0
        pass

    def getFormattedTime( self ) -> str :
        elapsedSecs = self.getElapsedSeconds()
        minutes , seconds = divmod( elapsedSecs , 60 )

        return f'{minutes:01}:{seconds:02}'

    def getElapsedSeconds( self ) -> int :
        elapsedSeconds = 0

        match self.state :
            case TimerState.Paused :
                elapsedSeconds = int( self.elapsedTime )
                pass
            case TimerState.Running :
                elapsedSeconds = int( self.elapsedTime ) + int( time.time() - self.startTime )
                pass
            case _ : # should never happen
                _logger.error( f'Unexpected TimerState: {self.state}' )
                pass

        return elapsedSeconds

