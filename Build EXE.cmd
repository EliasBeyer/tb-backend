pip install -r ./requirements.txt
pip install pyinstaller

pyinstaller^
    --distpath "."^
    --workpath "./_build"^
    --name "timebuddy"^
    --clean^
    --onefile^
    --windowed^
    --add-data "./app-content-pages/;./app-content-pages/"^
    --collect-binaries clr_loader^
    --i "./assets/logo_timebuddy.ico"^
    --runtime-hook "./assets/runtime_hook.py"^
    "./main.py"

@rd /S /Q "./_build"
@del *.spec
@del *.notanexecutable

pause